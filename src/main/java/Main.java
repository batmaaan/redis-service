import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.time.Duration;

public class Main {
    public static void main(String[] args) throws JsonProcessingException, InterruptedException {
        RedisClient redisClient = RedisClient.create("redis://password@localhost:6379/0");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String, String> syncCommands = connection.sync();
        final ObjectMapper objectMapper = new ObjectMapper();

        String firstKey = "key";
        syncCommands.set(firstKey, "Hello, Redis!");
        syncCommands.expire(firstKey, Duration.ofSeconds(10));


        System.out.println("check and get value from fistKey BEFORE expire=" + syncCommands.get(firstKey));
        Thread.sleep(10_000);
        System.out.println("check and get value from fistKey AFTER expire=" + syncCommands.get(firstKey));

        System.out.println();
        System.out.println("==================================================================");
        System.out.println();


        String newsKey = "news-1";
        News attention = new News("ТРЕВОГА!!!", "ВОЛК УКРАЛ КОЗЛЯТ!!!");
        String jsonValue = objectMapper.writeValueAsString(attention);
        syncCommands.set(newsKey, jsonValue);
        syncCommands.expire(newsKey, Duration.ofSeconds(10));

        System.out.println("check and get value from newsKey BEFORE expire=" + syncCommands.get(newsKey));
        Thread.sleep(10_000);
        System.out.println("check and get value from newsKey AFTER expire=" + syncCommands.get(newsKey));

        System.out.println();
        System.out.println("==================================================================");
        System.out.println();

        String key3 = "key3";
        syncCommands.set(key3, "some value");
        Thread.sleep(10_000);

        System.out.println("check key3 exist=" + syncCommands.get(key3));
        syncCommands.del(key3);
        System.out.println("check key3 not exist after remove=" + syncCommands.get(key3));


        System.out.println();



        connection.close();
        redisClient.shutdown();
    }
}
